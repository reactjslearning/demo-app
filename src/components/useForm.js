import { useState } from 'react'
const useForm = (callback) => {

    const [values, setValues] = useState({});

    const onChangeHandler = (e) => {
        e.persist();
        setValues({
            ...values,
            [e.target.name]: e.target.value
        })
    }
    const onSubmitHandler = (e) => {
        e.preventDefault();
        callback();
    }
    const onResetHandler = (e) => {
        Object.keys(values).forEach((key) => {
            setValues({
                [key]: ''
            })
        })
    }
    return {
        onSubmitHandler,
        onChangeHandler,
        onResetHandler,
        values
    }
}
export default useForm;