import React from 'react'
import Navbar from './navbar';
import Box from '@material-ui/core/Box'
import Button from '@material-ui/core/Button'

const Home = () => {
    return (
        <div>
            <Navbar />
            <h4>Home page</h4>
            <Box height={100} bgcolor="background.paper">

                <Box border="4px solid" m={2} clone>
                    <Button variant="contained" >Hello</Button>
                </Box>
                <Box border="4px solid" ml={[1,2,3,4,5]} clone>
                    <Button variant="contained" >Hello 2</Button>
                </Box>
            </Box>
        </div>
    )
}

export default Home
