
import React, { useContext } from 'react'
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import { makeStyles } from '@material-ui/core/styles';
import UserContext from '../context';

const useStyles = makeStyles(theme => {
    debugger
    return {
        root: {
            flexGrow: 1,
        },
        menuButton: {
            marginRight: theme.spacing(2),
        },
        title: {
            flexGrow: 1,
        },
    }
});
const Navbar = () => {
    const classes = useStyles();
    const { isLoggedIn, login, email } = useContext(UserContext);
    return (
        <div className={classes.root}>
            <AppBar position="static">
                <Toolbar>
                    <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
                        <MenuIcon />
                    </IconButton>
                    <Typography variant="h6" className={classes.title}>
                        News
                     </Typography>
                    {isLoggedIn && (
                        <Typography variant="h6" className={classes.title}>
                            {email}
                        </Typography>
                    )}
                    <Button onClick={login} color="primary">{isLoggedIn ? 'Logout' : 'Login'}</Button>
                </Toolbar>
            </AppBar>
        </div>
    )
}

export default Navbar
