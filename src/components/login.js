import React, { useContext } from 'react'
import { Container } from '@material-ui/core'
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import UserContext from '../context';
import { Redirect } from 'react-router-dom';
import useForm from './useForm'
const Login = () => {
    const { values, onChangeHandler,onResetHandler, onSubmitHandler } = useForm(onSubmit);
    const { login, isLoggedIn } = useContext(UserContext)
    function onSubmit(e) {
        login(values.email);
    }

    return (
        isLoggedIn ? <Redirect to="/home" /> :
            <Container maxWidth="xs" style={{ height: '400px', display: 'flex', flexDirection: 'column', justifyContent: 'center', justifyItems: 'center' }} >
                <form onSubmit={onSubmitHandler}>
                    <Typography component="h4" variant="h4">
                        Login to your account
              </Typography>
                    <div style={{ display: 'flex', flexDirection: 'column', marginTop: '40px' }}>
                        <TextField
                            id="Email"
                            label="Email"
                            name="email"
                            margin="dense"
                            value={values.email ? values.email : ''}
                            variant="outlined"
                            onChange={onChangeHandler}
                        />
                        <TextField
                            id="Password"
                            label="Password"
                            type="password"
                            margin="dense"
                            name="password"
                            value={values.password ? values.password : ''}
                            variant="outlined"
                            onChange={onChangeHandler}
                        />
                    </div>
                    <div style={{ display: 'flex', marginTop: '20px' }}>
                        <Button type="submit" variant="outlined" color="primary" >
                            Login
                    </Button>
                        <Button type="button"  onClick={onResetHandler} variant="contained" style={{ marginLeft: '10px' }} color="primary" >
                            Clear
                </Button>
                    </div>
                </form>
            </Container>
    )
}

export default Login
