import React, { useState } from 'react'
import Login from './components/login';
import { BrowserRouter as Router, Route } from 'react-router-dom'
import Home from './components/home';
import PrivateRoute from './components/private-route';
import { UserProvider } from './context';
import { ThemeProvider } from '@material-ui/styles';
import { createMuiTheme } from '@material-ui/core/styles';
const theme = createMuiTheme({
  palette: {
    type: 'dark',
  },
});
console.log(theme)
const App = () => {
  const [isLoggedIn, setLoggedIn] = useState(false);
  const [email, setEmail] = useState('');
  const login = (email) => {
    setLoggedIn(!isLoggedIn);
    setEmail(email);
  }
  return (
    <ThemeProvider theme={theme}>
      <UserProvider value={{ isLoggedIn, login, email }}>
        <Router>
          <PrivateRoute path="/" exact component={Home} />
          <Route path="/login" component={Login} ></Route>
          <PrivateRoute path="/home" component={Home} />
        </Router>
      </UserProvider>
    </ThemeProvider>
  )
}

export default App

